import React from 'react'
import { Container,Button,Badge,Table } from 'react-bootstrap'

function TableData(props) {

    return (
        
        <Container>
            <Button variant="primary" className="my-3">Reset</Button>
            <Badge variant="warning" className="mx-2">count</Badge>
        <Table>
            <thead>
                <tr>
                <th>#</th>
                <th>Food</th>
                <th>Amount</th>
                <th>Price</th>
                <th>Total</th>
                </tr>
            </thead>
            </Table>
        </Container>
    )
}
export default TableData
